#include<stdio.h>
#include<signal.h>
#include<unistd.h>
/*le pere affiche les entier paires zt le fils les entier impair a laide de forkk() et les signaux */
 void f()
{
printf("\n");
}
int main()
{
int pid;
int i;
signal(SIGUSR1,f);
pid=fork();
if(pid==0)
{ /*fils*/
   pid=getppid();
 for(i=1;i<=100;i+=2)
    { 
      printf("%d\n",i);
      kill(pid,SIGUSR1);
         pause();
    }
}
else { /*le pere*/
      for(i=2;i<=100;i+=2)
         { 
           pause();
           printf("%d\n",i);
           kill(pid,SIGUSR1);
         }
}
return 0;
}
         
